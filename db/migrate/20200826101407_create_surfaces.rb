class CreateSurfaces < ActiveRecord::Migration[5.2]
  def change
    create_table :surfaces do |t|
      t.string :name
      t.float :slowing_percentage
      t.integer :track_id

      t.timestamps
    end
  end
end
