# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Car.create(
	[
		{
			id: 1,
			car_slug: "subaru_impreza",
			max_speed: 280,
			tires: nil
		},
		{
			id: 2,
			car_slug: "Mercedes-Benz",
			max_speed: 300,
			tires: nil
		},
		{
			id: 3,
			car_slug: "BMW",
			max_speed: 320,
			tires: nil
		}
	]
)

@track1 = Track.create(id: 1, name: "Nurburgring", time_zone: "Harare")
@track2 = Track.create(id: 2, name: "Sydney Motorsport Park", time_zone: "Sydney")
@track3 = Track.create(id: 3, name: "Circuit Gilles Villenaeuve Montreal", time_zone: "Eastern Time (US & Canada)")
@track4 = Track.create(id: 4, name: "Guia Circuit", time_zone: "Beijing")


Surface.create(
	[
		{
			id: 1,
			name: "snow",
			slowing_percentage: 35,
			track_id: @track1.id
		},
		{
			id: 2,
			name: "gravel",
			slowing_percentage: 12,
			track_id: @track2.id
		},
		{
			id: 3,
			name: "asphalt",
			slowing_percentage: 15,
			track_id: @track3.id
		}
	]
)