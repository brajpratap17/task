class Track < ApplicationRecord

	has_one :surface

	# TRACKS = [
	# 	{
	# 		name: "Nurburgring",
	# 		surface_type: snow
	# 	}
	# ]

	def calculate_speed(car)
		(car.max_speed - ((car.max_speed * self.surface.slowing_percentage) / 100.0))
	end
end
