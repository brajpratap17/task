class Car < ApplicationRecord
	def details(track:)
		response = { 
			car: {
				id: self.id, 
				car_slug: self.car_slug, 
				max_speed: "#{self.max_speed}km/h"
			} 
		}
		response[:car][:max_speed_on_track] = self.max_speed_on_track(track: track)
		response
	end

	def max_speed_on_track(track:)
		speed_on_track = "nil"
		if track.blank?
			speed_on_track = "no track selected"
		else
			@track = Track.find_by_name(track)
			if @track.present?
				speed_on_track = "#{@track.calculate_speed(self)}km/h"
			else
				speed_on_track = "track not found"
			end
		end
		speed_on_track
	end
end
