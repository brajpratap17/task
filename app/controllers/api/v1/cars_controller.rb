class Api::V1::CarsController < ApplicationController

	def find_cars
		@car = Car.find_by_car_slug(params[:name][1..-1])
		if @car.present?
			render json: @car.details(track: params[:track])
		else
			render json: {message: "car not found"}
		end
	end

	private

	def car_params
    params.require(:car).permit()
  end

end