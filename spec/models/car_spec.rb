require 'rails_helper'

RSpec.describe Car, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"

  before(:each) do
    @car = Car.find 1
  end

  it "should get car details with max speed limit" do
    response = @car.details(track: "Nurburgring")
    expect(response).to eq({"car":{"id":1,"car_slug":"subaru_impreza","max_speed":"280km/h","max_speed_on_track":"182.0km/h"}})
  end

  it "should get car details without track" do
    response = @car.details(track: "abc")
    expect(response).to eq({"car":{"id":1,"car_slug":"subaru_impreza","max_speed":"280km/h","max_speed_on_track":"track not found"}})
  end

  it "should get car details without track param" do
    response = @car.details(track: nil)
    expect(response).to eq({"car":{"id":1,"car_slug":"subaru_impreza","max_speed":"280km/h","max_speed_on_track":"no track selected"}})
  end

  it "should get max speed of car on a track" do
    response = @car.max_speed_on_track(track: "Nurburgring")
    expect(response).to eq("182.0km/h")
  end

  it "should get no track found" do
    response = @car.max_speed_on_track(track: "no track")
    expect(response).to eq("track not found")
  end

  it "should get no track selected" do
    response = @car.max_speed_on_track(track: nil)
    expect(response).to eq("no track selected")
  end

end
