require 'rails_helper'

RSpec.describe Track, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"

  before(:each) do
    @car = Car.find 1
    @track = Track.find 1
    @surface = Surface.find 1
  end

  it "should validate max speed limit" do
    max_speed = @track.calculate_speed(@car)
    expect(max_speed).to eq(182.0)
  end

  it "should not validate max speed limit" do
    max_speed = @track.calculate_speed(@car)
    expect(max_speed).not_to eq(181.0)
  end

  it "should have the right associated surface" do
  	expect(@surface.track_id).to eq(@track.id)
    expect(@track.surface).to eq(@surface)
  end

end
